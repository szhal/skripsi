**CONCLUSION**

**KUBERNETES SECURITY CHALLENGE**
1. Explosion of East-West Traffic
   Containers might be seamlessly deployed through hosts or perhaps even clouds, increasing the east-west or internal traffic that needs to be monitored for attacks significantly.
2. Increased Attack Surface
   *Every container can have a specific surface area of attack and weaknesses that can be exploited*. Additionally, attention should be extended to the additional attack surface introduced by container orchestration platforms such as Kubernetes and Docker
3. Automating Security to Keep Pace
   Old protection models and software won't be able to keep up in an ever-changing container environment.

**SECURITY PRINCIPLE IN CONTAINER**
1. Defense in Depth
2. Least Privilege
3. Reducing the Atttack Surface
   As a general rule, the more complex a system is, the more likely it is that there is a way to attack it. Eliminating complexity can make the system harder to attack. This includes:
   • Reducing access points by keeping interfaces small and simple where possible
   • Limiting the users and components who can access a service
   • Minimizing the amount of code

*Container Diagram*
1. Container Daemon
2. Container Network
3. Local Image Registry
4. Public Image Registry
5. Application inside container

*Container Attack Surface*
1. OS and Kernel Attack
   1. Denial of Service Attack
   2. Misconfigurations
      a. Privilege Mode
      b. Docker Socket Misconfigurations
   3. Kernel Exploit
2. Network-based Attack
   1. Unauthenticated Docker REST HTTP REST API
   2. No SSL by default
   3. No network segregation
3. Daemon-based Attack
4. Image-based Attack
5. Application-based Attack


*Surface:*
1. Linux Namespace
   a. Unix Timesharing System (UTS)
   b. Process IDs
   c. Mount Namespace / Mount Point
   d. Network Namespace
   e. User namespace (User and group IDs)
   f. Inter-process communication (IPC)
   g. cGroups Namespace


1. Menghabiskan resource
2. Merusak node
3. Meneruskan trafik ke DNS 
4. Merubah hostname
5. 




IMAGE REGISTRY
Pull image dari trusted registry

BASE-IMAGE
Membuat image seminimal mungkin, distroless

DOCKERFILE
Menggunakan metode "Multi-Stage Build"


Limiting attack surface di container mengarah kepada:
