**Attack Surface**
1. Attack surface is defined as the total number of *all possible entry points* for unauthorized access into a system.
2. It includes all *vulnerabilities* and *endpoints* that can be exploited to carry out a security attack.
3. The attack surface is also the *entire area* of an organization ot system that is accceptible to hacking. 
4. The large number of *devices*, *web applications*, and *network nodes* create many *potential cybersecurity threats*

**Different Types of Attack Surfaces**
1. Digital Attack Surfaces
    a.endpoint
      - applications, 
      - code, 
      - ports, 
      - servers,
      - websites, and
      - unauthorized system access points
    b.vulnerability:
      - poor coding,
      - weak password,
      - default operating system ettings,
      - exposed application programming interfaces,
      - poor maintained software
2. Physical Attack Surfaces
    a.endpoint devices:
      - laptops,
      - mobile devices,
      - USB ports
    b.vulnerability (caused by carelessness or unawareness)
      - improperly discarded hardware that contains sensitive data
      - passwords on paper
      - physical break
3. Both physical and digital attack surfaces *should be limited* in size to *protect surfaces from anonymous, public access*.

**Attack Surface Management**
1. Attack surface management refers to the *continuous surveillance and vigilance* required to mitigate *all current and future cyberthreats*.
2. It includes all:
   a. risk assessments, 
   b. security controls, 
   c. security measures that go into:
      -> mapping and protecting the attack surface, 
      -> and mitigating the chances of a successful attack
![](2022-08-01-20-28-18.png)
3. Key questions answered in attack surface management:
   a. What are the *high-risk areas* and *vulnerabilities* in the system?
   b. Where can new attack vectors be created due to *system changes*?
   c. How can the system be protected from cyber attacks?

*How can the attack surface be limited?*
1. Access Control
   limit access to sensitive data and resources both internally and externally
2. Complexity Elimination
   - Eliminate unnecessary or unused software (can result in policy mistakes and enabling bad actors), 
   - All system functionality must be assessed and maintained regularly.
3. Regular Scanning
4. Network Segmentation
   - implement firewalls
   - microsegmentation (is a technique used to divide a network into logical and secure units)

**Difference between an attack surface and an attack vectors**
1. Attack surface is what is being attacked
2. Attack vector is pathway or method used by a hacker to illegally access a network or computer to exploit system vulnerabilities. Such as:
   - Cyber attack: deliberate attack to gain unauthorized access
   - Network data interception: the way to extract sensitive information directly from network
   - Data breaches: rogue employees, social engineering, unauthorized spy workers

*Manage Digital Attack Surfaces*
*Manage Physical Attack Surfaces*

techtarget.com/whatis/definition/attack-surface


