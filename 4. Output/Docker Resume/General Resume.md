# **What is Namespace?**
- `Namespace` is a part of Linux kernel
- `Namespace` is a feature of the Linux kernel that partitions kernel resources
- One set of process sees one set of resources while another set of processes sees a different set of resource
- In other words, the key feature of `namespaces` is that they isolate processes from each other.

## **Types of Namespaces**
- A user namespace
- A process ID (PID) namespaces
- A network namespace
- A mount namespace
- An interprocess communication (IPC) namespace
- A Unix Time Sharing (UTS) namespace

# **What is cgroups**
- A control groups (cgroup) is a Linux kernel feature that limits (membatasi), accounts for (memperhitungkan), and isolate (mengisolasi) the resource usage (CPU, memory, disk I/O, network, and so on) of a collection of processes.

- Cgroups provide the following feature:
  - Resource limit
  - Prioritization
  - Accounting
  - Control (status: frozed, stopped, restarted)

# **What is chroot?**
- Chroot, simply, `run a command with a different root directory.
- The main thing chroot is used for is locking away system daemons so that any security vulnerabilities in those daemons don’t affect the rest of the system.

# **What is Service Discovery**
- Service discovery is a mechanism by which services discover each other dynamically without the need for hard coding IP addresses or endpoint configuration
- In modern cloud-native infrastructure such as Kubernetes, applications are designed using microservices.
- The different components need to communicate within a microservices architecture for applications to function, but individual IP addresses and endpoints change dynamically
- As a result, there is a need for `service discovery` so services can automatically discover each other.
![](2022-08-31-18-25-39.png)

## **Types of Service Discovery**
1. Server side service discovery
2. Service registry

https://www.densify.com/kubernetes-autoscaling/kubernetes-service-discovery#:~:text=What%20is%20Service%20Discovery%3F,applications%20are%20designed%20using%20microservices.

# **What is Promiscuous Mode?**
- In computer networking, promiscuous mode is a mode of operation, as well as security, monitoring and administration technique.
- In promiscuous mode, a network device, such as an adapter on a host system, can intercept and read in its entirely each network packet that arrives.
- This mode applies to both a wired NIC and wireless NIC. In both cases, it causes the controller to pass all traffic it receives to the central processing unit instead of just the frames it is especially programmed to receive.
- This enables network monitoring tool to examine the content of the transmission for potential threats.
- Promiscuous mode ensure that every data packet that is transmitted is received and read by a network adapter. This means the adapter doesn't filter packets. Instead, it passes each packet on to the operating system or any monitoring application installed on the network.

# **What is cloud native architecture?**
Cloud native architecture concerns the design of applications or services that were made specifically to exist in the cloud, rather than in a more traditional on-premises infrastructure. A successful cloud native architecture needs to be easy to maintain and supported by a next-generation cloud, while also being cost efficient and self-healing. Compared to legacy systems, cloud native architectures have a greater level of flexibility, without having to rely on physical servers.

This is where microservices and serverless functions can play a large and important role. Microservices are the core of cloud native application architecture, and they have become a key tool for companies that are making the move to the cloud. Microservices arrange an application into multiple, independent services, each of which serves a specific function. Many software companies take advantage of microservices because they support DevOps, enable flexibility, and improve scalability, while also reducing costs. Cloud native microservices communicate with each other via APIs and use event-driven architecture, which serves to enhance the overall performance of each application. Oracle Cloud Native services follow the CNCF trail map to help simplify the journey and make it easier for companies to start building, deploying, and managing modern cloud native applications.
![](2022-09-01-16-44-49.png)

## **Function**

The term **serverless functions** describes an architecture style that focuses on increasing developers’ productivity. A serverless application lets you write code on a platform that functions as a service (FaaS) using event-driven architectures and various backend-as-a-service (BaaS) models. This eliminates the need to worry about provisioning, patching, scaling, security, high availability, and so forth. With FaaS platforms, such as Oracle Functions, applications are broken up into small pieces of code (nanoservices), which are dynamically scheduled and run on demand when triggered by an event. The advantage of this approach is that code is called and executed only when needed, and you pay only for the resources used during the execution duration. This differs from a classic server approach, in which applications are loaded into a server and spend most of their time idle, waiting for requests. Thus, in serverless computing, you pay only for computing resources you actually use, rather than paying for idle resources.

# **What are Metrics?**
Metrics are a quantitative and qualitative way to verify a desired behaviour. For example: many operators like to measure their "uptime", a count to measure of how to often services are available for users. Network metrics are similar, and are related to desired outcomes. Some measurement include:
-  

- **Network capacity:** how much traffic can cross a given link (kb, mb, gb)
- **Network utilization:** how much capacity is currently in use (kb, mb, gb)
- **Throughput:** *(sometimes called achievable bandwith)* how much of the network can be used at a given time (kbps, mbps, gbps)
- **One-way latency:** how long data takes to travel from one host to another
- **Round-trip time:** _(or two-way latency)_ how long data takes to travel from one host to another and back to the first host
- **Packet loss:** how many packets are dropped for any reason on a network segment or path
- **Packet duplication:** how many packets are duplicated for any reason on a network segment or path
- **Jitter:** is the variation in arrival times for packets between two participating endpoints

Defining network metrics that matter to an organization depend on the use case. If someone wants to perform the task of bulk data movement, often it desirable to have a path high throughput. This implies other desirable features:
- Low (or zero) packet loss
- Stable jitter
- Low path utilization