# **Docker Network - Documentation**

## **Networking Overview**
- One of the reason Docker containers and services are so powerful is that _**you can connect them together**_, `or connect them to non-Docker workloads.`
- Docker containers and services don't even need to be aware that they are deployed on Docker, or whether their peers are also Docker workloads or not.
- Whether your Docker host run Linux, Windows, or a mix of the two, **_you can use Docker to manage them in a platform-agnostic way._**

## **Network Drivers**
Several drivers exists by default, and provide core networking functionality:
- `bridge`: The default network driver. If you don't specify a driver, this is the type of network you are creating. **_`Bridge` networks are usually used when your application run in standalone containers that need to communicate._**
- `host`: For standalone containers, remove network isolation between the container and the Docker host, and use host networking directly.
- `overlay`: **Overlay connect multiple Docker daemon together and enable swarm services to communicate with each other.** You can also use overlay networks to facilitate communication between a swarm service and a standalone container, or between two standalone containers on different Docker daemons. This strategy removes the need to do OS-level routing between these containers.
- `ipvlan`: **IPvlan networks give users total control over both IPv4 and IPv6 addressing**. The VLAN driver builds on top of that in giving operators complete control of layer 2 VLAN tagging and even IPvlan L3 routing for users interested in underlay network integration.
- `macvlan`: **Macvlan networks allow you to assign a MAC address to a container, making it appear as a physical device on your network.** Using the `macvlan` driver is sometimes the best choice when dealing with legacy applications that expect to be directly connected to the physical network, rather than routed through the Docker host's network stack.
- `none`: For this container, disable all networking. Usually used in conjunction with a custom network driver. `none` is not available for swarm services


## **Network driver summary**
- `User-defined bridge networks` are best when you need multiple containers to communicate on the same Docker host.
- `Host networks` are best when stack should not be isolated from the Docker host
  > ### **💡 What are the Use Cases of Docker Network Host?**
  > Docker network **`host`** can _**offer performance improvements** **and optimizations**_ over other Docker networking modes (e.g., "none" and "bridge" modes). In addition, **_Docker `host` networking doesn't require network address translation (NAT)_**, making it easier to handle a large number of ports simultaneously. However, users need to take care to avoid port conflicts while in Docker host networking mode.
- `Overlay networks` are best when you need containers running on different Docker hosts to communicate , or when multiple applications work together using swarm services.
- `Macvlan networks` are best when you are migrating from a VM setup or need your containers to look like physical hosts on your network, each with unique MAC address.
- `Third-party network plugins` allow you to integrate Docker with specialized network stacks.
> 💡 In Docker, the _`host`_ is a machine responsible for running one or more containers. Docker network host, also known as _`Docker host networking`_, is a networking mode in which a Docker container shares its network namespace with the host machine.

## **Use bridge networks**
- a bridge network is a **Link Layer device** which forwards traffic between networks segments. **_A bridge can be a hardware device or a a software device_** running within a host machine's kernel.
- a bridge network uses a software bridge which allows containers connected to the same bridge network to communicate, while providing isolation from containers on different bridge networks cannot communicate directly with each other.
- Bridge networks apply to containers running on the **same** Docker daemon host. For communicating among containers running on different Docker daemon hosts, can using either 
- when start docker, a `default bridge network` (also called `bridge`) is created automatically.

## **Differences between user-defined bridges and the default bridge**
- User-defined bridges provide `automatic DNS resolution` between container
- User-defined bridges provide **_better isolation_**
- Containers can be attached and detached from user-defined on the fly **_(plug and play)_**
- Each user-defined network **creates a configurable bridge**
- Linked containers on the default bridge network **share environment variables.**

## **Manage a user-defines bridge**
- use `docker network create` command to create a user-defined bridge network
  ```sh
  $ docker network create my-net
  ```
  > 💡 _you can specify the subnet, the IP address range, the gateway, and other options. Reference see `docker network create --help` for details._
- use the `docker network rm` command to remove a user-defined bridge network. If containers are currently connected to the network, `disconnect` them first.
  ```sh
  $ docker network rm my-net
  ```
  ![](2022-08-26-13-42-08.png)

## **Connect/Disconnect a Container to a user-defined bridge**
- when you create a new container, you can specify one or more `--network` flags.
  ```sh
  $ docker create --name my-nginx \
    --network my-net \
    --publish 8080:80 \
    nginx:latest
  ```
- to connect a `running` container to an existing user-defined bridge, use `docker network connect` command.
  ```sh
  $ docker network connect my-net my-nginx
  ```
- to disconnect a running container from a user-defined bridge, use `docker network disconnect` command
  ```sh
  $ docker network disconnect my-net my-nginx
  ```
## **Launch a Container on the Default Network**
- network driver in docker is a term of `network type`
- by default, docker provides two network drivers `bridge` and `overlay` drivers
- you can also write a network driver plugin so that you can create your own drivers
- every installation of docker engine automatically includes three default networks
  ```sh
  $ docker network ls
  
  NETWORK ID          NAME                DRIVER
  18a2866682b8        none                null
  c288470c46f6        host                host
  7b369448dccb        bridge              bridge
  ```
- the network name `bridge` is a special network. Docker always launches your container in this network
- test to launch container with ubuntu image by the name of networktest
  ```sh
  $ docker run -itd --name networktest ubuntu
  ```
- Inspecting the network is an easy way to find out the container’s IP address.

  ![](2022-08-24-21-01-18.png)
- `docker0` is a virtual bridge interface created by Docker. It randomly chooses an address and subnet from a private defined range. _**All the docker containers are connected to this bridge**_ and use `NAT` rules to communicate with the outside world.

  ![](2022-08-24-20-54-20.png)

- You can remove a container from a network by disconnecting the container from its network.
  ```sh
  $ docker network disconnect bridge networktest
  ```
- you cannot remove the builtin `bridge` network named `bridge`. **_Networks are natural ways to isolate containers_** from other containers or other networks.

## **Create Your Own Bridge Image**
- Docker engine natively supports both **bridge networks** and **overlay networks**
- A bridge network is limited to a `single host` running Docker Engine
- An overlay network can include `multiple hosts`
- Example create a bridge network :
  ```sh
  $ docker network create -d bridge my_bridge
  ```
- `-d` means `driver` or network driver or network type
  ```sh
  ubuntu@docker:~$ docker network ls
  NETWORK ID     NAME        DRIVER    SCOPE
  0aa2a46fd472   bridge      bridge    local
  7387b42b16da   host        host      local
  75eb8cbc46b3   my_bridge   bridge    local
  efbdf0e4b88a   none        null      local
  ```
- if you inspect the network, it has nothing in it

  ![](2022-08-24-21-29-47.png)

## **Add Containers to a Network**
- to build web applications that act in concert `[re: barengan]` and do securely, create a network. Networks provide fully isolation for containers. You can add containers to a network when you first run a container.
- Launch a container running a PostgreSQL database and pass it the `--net=my_bridge` flag to connect it to your network
  ```sh
  $ docker run -d --net my_bridge --name db training/postgres
  ```

  ![](2022-08-24-21-46-27.png)

- Now, go a head and start lauch a new familiar web application without specify a network
  ```sh
  docker run -d --name web training/webapp python app.py
  ```

  ![](2022-08-24-21-52-24.png)

  >💡 *It is running in the default `bridge` network*
- Now, open a shell to your running `db` container:
  ```sh
  docker container exec -it db bash
  ```

  ![](2022-08-24-22-20-54.png)

  > _📌 Notice that `ping` failed because the tow containers are running on different networks_
- fix that by connecting web containers to the same network as db, `my_bridge`
  ```sh
  $ docker network connect my_bridge web
  ```

  ![](2022-08-24-22-25-44.png)

  > _📌 Docker networking allows you to attach a container to as many networks as you like. You can also attach an **already running container.**_
- open a shell into the `db` application again and try the `ping` command

  ![](2022-08-24-22-28-49.png)

  > _📌 The `ping` shows it is contacting a different IP address, the address on the `my_bridge` which is different from its address on the `bridge` network._

## **Use host networking**
- if you use `host` networking mode for a container:
  1. that container's network stack **is not isolated from the Docker host** (the container shares the host's networking namespace), and
  2.  the container **doesn't get its own IP-address allocated**
  3. for instance, if you run a container which binds to port 80 (and use `host` networking), the container's application is available on port 80 on the host's IP address
![](2022-08-27-12-18-54.png)
![](2022-08-27-12-19-17.png)
![](2022-08-27-12-20-15.png)

## **Use macvlan networks**
- some applications, especially legacy applications or applications which monitor network traffic, expect **to be directly connected to the physical network.** 
- in this type of situation, you can use `macvlan` network driver **to assign a MAC address to each container's virtual network interface.** **_Making it appear to be a physical network interface directly connected to the physical network._**
- in this case, you need to designate a physical interface on your Docker host to use for the `macvlan`, as well as the subnet and gateway of the `macvlan`.
- you can even isolate your `macvlan` networks using different physical network interfaces.

## **Create a macvlan network**
when you create a `macvlan` network, **it can be either be in bridge mode or 802.1q trunk bridge mode.**
- **in bridge mode**, `macvlan` traffic goes through a physical device on host.
- **in 802.1q trunk bridge mode**, traffic goes through an 802.1q sub-interface whic Docker creates on the fly. This allows you to control routing and filtering at a more granular level

### **Bridge mode**
- to create a `macvlan` network which bridges with a given physical network interface, use `--driver macvlan` with the `docker network create` command.
  ```sh
  $ docker network create -d macvlan \
    --subnet=172.16.86.0/24 \
    --gateway=172.16.86.1/24 \
    -o parent=eth0 pub_net
  ```
### **802.1q trunk bridge mode**

<br></br>

# **Network Chunk - Docker Networking**
## **Default Bridge**
- once install docker, there is `docker0` interface appear
![](2022-08-25-13-35-10.png)
- `docker0` is a new virtual bridge interface. It is the default interface and network for the default bridge
![](2022-08-25-13-31-05.png)
- by default, every launched container attachs to default `bridge`
![](2022-08-25-13-40-26.png)
- container get ip address from `bridge` by **dhcp**
![](2022-08-25-13-41-55.png)
- `bridge` contains **DNS address** of all containers name, also called `container to container DNS action` so that every single container in same bridge interface can `ping` each other by its **ip or domain name** (container's name)
- bridge connect to outside network through `NAT Masquerade` so that it can access the internet
- if we want to access container (eg. website by nginx) from the internet, it won't work by default (because of NAT). **_You have to maually expose those port_**

## **User-Defined Bridge**
- user-defined bridge is a bridge network driver that we make it, not the default one.
  ```sh
  $ sudo docker network create drivertest
  ```
  or
  ```sh
  $ docker network create -d bridge drivertest
  ```
- user-defined bridge is preferred by docker because its isolation. It's protected from the default network. They can't talk each other
  ![](2022-08-25-14-44-33.png)
<br><br/>
# **Learning Docker Networking - Book**
## **Preface**
Docker provides the networking primitives that allows administrators to specify:
1. how different containers network with each application
2. how connect to each of their components
3. how to distribute them accross a large number of servers
4. ensure coordination between them irrespective of the host or the VM that they are running on.

## **Docker Networking Primer**
-

# **Docker Networking and Service Discovery**
