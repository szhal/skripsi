# **DOCKER RESUME**

## **1. Apa itu Docker?**
### **Learning Docker**
Bab Getting Started with Docker

```h
Docker is an open source containerization engine, which automates the packaging, shipping, and deployment of any software applications that are presented as lightweight, portable, and self-sufficient containers, that will run virtually anywhere
```
```h
Docker container is a software bucket comprising everything necessary to run the software independently. There can be multiple Docker containers in a single machine and containers are completely isolated from one another as well as from the host machine
```
![](2022-08-19-16-41-43.png)
![](2022-08-19-16-43-45.png)

## **2. Kenapa harus Docker?**
### **Mastering Docker**
Bab Docker Overview - Understanding Docker

**1. Developers**

   Problem: `Mismatch environment between developer laptop and production server`
   
   ![](2022-08-19-16-15-51.png)

   Solution: `Each environment can be wrapped quickly in a container with the same as production server environment`
   ![](2022-08-19-16-18-46.png)

**2. Operator**

   Problem: `Mismatch environment between one application and another in production server, and can affect availability issue`
   ![](2022-08-19-16-29-04.png)
   ![](2022-08-19-16-29-43.png)

   Solution: `Each application can be launched, scaled, and routed alone without reducing the availability`
   ![](2022-08-19-16-30-39.png)
   
**3. Enterprise**

   Problem: `application downtime impact enterprise revenue and reputation. testing for every single deployment take more time depending on complexity of the application`

   ![](2022-08-19-16-32-54.png)

   Solution: `developer already checks their code on their local development environment, manually or automatically (by testing tool). testing process and procedures are a lot more flexible, the testing environment can be removed to free up resource for next lot of tests, rather than redeploying or re-imaging servers for the next set of testing`

   ![](2022-08-19-16-33-49.png)

## **3. Perbedaan Docker dengan Virtualization?**
### **Learning Docker**
![](2022-08-19-16-50-22.png)
![](2022-08-19-16-50-45.png)

## **4. Perbedaan High Availability dan Fault Tolerance**
### **High Availability**
**`High availability`** `is the ability of a workload to remain operational, with minimal downtime, in the event of a disruption. Disruptions include hardware failure, networking problems or security events, such as DDoS attacks.`
```
In a highly available system, workloads are spread across a cluster of servers. If one server fails, the workloads running on it automatically move to other servers.

Kubernetes is an example of a platform on which most workloads are considered highly available. As long as a Kubernetes cluster contains more than one worker node, containerized applications and application services in pods can automatically restart on a different node if the original node fails.
```

![](2022-08-20-11-03-20.png)

### **Fault Tolerance**
**`Fault tolerance`** `is the ability of a workload to remain operational with zero downtime or data loss in the event of a disruption.`

```
In a fault-tolerant environment, instances of the same workload are typically hosted on two or more independent sets of servers. Admins keep the instances continuously in sync, so data and application state are identical across each instance. If one instance fails, another instance takes over. In this way, fault tolerance prevents user disruption and data loss -- assuming that at least one of the instances remains available.

As an example of fault tolerance, admins could configure an object storage bucket to replicate data automatically to a separate bucket. If the first bucket fails, the second bucket accommodates all requests.
```

![](2022-08-20-11-04-24.png)

### **High Availability vs Fault Tolerance**
**1. Level of Disruption**

*`With high availability, a workload usually experiences some level of disruption when a failure occurs`.* It may take seconds or minutes to migrate a workload from a failed server to a new one in a highly available cluster. Because the new server likely does not have identical copies of the failed server's data, there may be some permanent data loss. *`In contrast, a successful fault-tolerant environment provides zero downtime and no data loss because both instances maintain identical copies of the data`*

**2. Infrastructure Requirement**

*`Fault-tolerant environments require IT organizations to mirror workloads on dedicated infrastructure.`* As a result, these environments double an organization's infrastructure footprint, in the cloud or on premises. In either deployment scenario, expect twice the hosting costs of a non-fault tolerant workload. *`Highly available environments are not as demanding, but they do require some extra infrastructure capacity.`* This makes highly available environments *`less expensive`* to operate than fault-tolerant ones.

**3. Management**

*`Fault-tolerant workloads are more challenging to set up and administer.`* To ensure fault tolerance, admins must keep two or more workload instances in sync. This mean that changes in one instance are implemented in the other instance instantaneously. In contrast, *`high-availability workloads are less complex to set up and manage.`*

The main benefit of both high availability and fault tolerance is *`protection against hardware failure`, `not software issues.`* If a running instance in a highly available cluster fails because of a buggy application on it, the new instance of that workload that replaces it likely will also fail for the same reason. Likewise, in a fault-tolerant environment, an application bug is likely to cause both workload instances to fail.

## **5. What are Virtual Machines (VMs)?**
  
  A virtual machine (VM) is an operating system that shares the physical resources of one server. It is a guest on the host’s hardware, which is why it is also called a guest machine.

  There are several layers that make up a virtual machine. The layer that enables virtualization is the hypervisor. A hypervisor is a software that virtualizes the server.

## **How a Virtual Machine Works**

  Everything necessary to run an app is contained within the virtual machine – the virtualized hardware, an OS, and any required binaries and libraries. Therefore, virtual machines have their own infrastructure and are self-contained.
  
  ![](2022-08-22-10-02-28.png)

  Each VM is completely isolated from the host operating system. Also, it requires its own OS, which can be different from the host’s OS. Each has its own binaries, libraries, and applications.

---
**Virtual machine monitor (VMM):** another name for the hypervisor

**Host machine:** the hardware on which the VM is installed

**Guest machine:** another name for the VM

---


## **Virtual Machine: PROS and CONS**
### **Virtual Machine: PROS**
VMs reduce expenses. Instead of running an application on a single server, a virtual machine enables utilizing one physical resource to do the job of many. Therefore, you do not have to buy, maintain and store enumerable stacks of servers.

Because there is one host machine, it allows you to efficiently manage all the virtual environments with the centralized power of the hypervisor. These systems are entirely separate from each other meaning you can install multiple system environments.

Most importantly, a virtual machine is isolated from the host OS and is a safe place for experimenting and developing applications.

### **Virtual Machine: CONS**
Virtual machines may take up a lot of system resources of the host machine, being many GBs in size. Running a single app on a virtual server means running a copy of an operating system as well as a virtual copy of all the hardware required for the system to run. This quickly adds up to a lot of RAM and CPU cycles.

The process of relocating an app running on a virtual machine can also be complicated as it is always attached to the operating system. Hence, you have to migrate the app as well as the OS with it. Also, when creating a virtual machine, the hypervisor allocates hardware resources dedicated to the VM.

A virtual machine rarely uses all the resources available which can make the planning and distribution difficult. That’s still economical compared to running separate actual computers.

### **Popular VM providers:**
  - VMware vSphere
  - VirtualBox
  - Zen
  - Hyper-V
  - KVM

## **6. What is a Container?**

A container is an environment that runs an application that is not dependent on the operating system. It isolates the app from the host by virtualizing it. This allows users to created multiple workloads on a single OS instance.

The kernel of the host operating system serves the needs of running different functions of an app, separated into containers. Each container runs isolated tasks. It cannot harm the host machine nor come in conflict with other apps running in separate containers.

## **How do Container Works**

  When working inside a container, you can create a template of an environment you need. The container essentially runs a snapshot of the system at a particular time, providing consistency in the behavior of an app.

  The container shares the host’s kernel to run all the individual apps within the container. The only elements that each container requires are bins, libraries and other runtime components.

  ![](2022-08-22-09-55-32.png)


## **Container: PROS and CONS**
### Container: PROS
  Containers can be as small as 10MB and you can easily limit their memory and CPU usage. This makes containers remarkably lightweight and fast to launch as opposed to deploying virtual machines, where 
  the entire operating system needs to be deployed.

  Because of their size, you can quickly scale in and out of containers and add identical containers.

  Also, containers are excellent for Continuous Integration and Continuous Deployment (CI/CD) implementation. They foster collaborative development by distributing and merging images among developers.

### Container: CONS

  A container uses the kernel of the host OS and has operating system dependencies. Therefore, containers can differ from the underlying OS by dependency, but not by type. The host’s kernel limits the use of other operating systems.

  Containers still do not offer the same security and stability that VMs can. Since they share the host’s kernel, they cannot be as isolated as a virtual machine. Consequently, containers are process-level isolated, and one container can affect others by compromising the stability of the kernel.

  Moreover, once a container performs its task, it shuts down, deleting all the data inside of it. If you want the data to remain on the host server, you have to save it using Data Volumes. This requires manual configuration and provisioning on the host.

## **How to Choose VMs and Containers**

Deciding whether to go for virtual machines or containers depends on the work you want your virtual environment to carry out.
### **Virtual machines are a better solution if you need to:**

  * Manage a variety of operating systems
  * Manage multiple apps on a single server
  * Run an app that requires all the resources and functionalities of an OS
  * Ensure full isolation and security

### **Containers are suitable if you need to:**

  - Maximize the number of apps running on a server
  - Deploy multiple instances of a single application
  - Have a lightweight system that quickly starts
  - Develop an application that runs on any underlying infrastructure

>💡 Note: VMs and containers should not necessarily be seen as rivals. Rather, you can use both to balance the workload between the two.

**Virtual machines are commonly used for** demanding applications, network infrastructure, and apps that will consume most of the resources of the VM.

**Containers are commonly used for** web, applications and caching services, network daemons, and small databases.
<br><br/>
# **REVIEW ARTICLE**
## **The Road to Docker: A Survey**

**I. Introduction**

- Docker is an open standard platform for building, exporting, and running applications.

- Docker provides facility to separate your applications from your infrastructure to deliver software quickly

- A Docker container is a software bucket comprising of all the supporting dependencies necessary to run the software independently

- Also the isolation and security feature in Docker allow you to run multiple containers simultaneously 
on a single host machine

  ![](2022-08-20-12-17-02.png)

**II. Docker Concept**

- Docker is a platform designed *`to make it easier to create, deploy, and run virtualized application containers`* on a common operating system (OS), with an ecosystem of allied tools.

- Initially, Docker was created to work on the Linux platform, but has extended to offer greater support for non-Linux operating platforms, including Mac OSX and Microsoft Windows.

- *`Before Docker, deploying software to different environments required huge efforts.`* Even though these efforts were encapsulated in virtual machines, a lot of time was spent in managing and deployment of these machines, waiting for them to install and boot, and managing the overhead of resource use they created.

  ![](2022-08-20-12-08-40.png)
  
  ![](2022-08-20-12-20-41.png)

III. Docker Architecture
  
  ![](2022-08-20-18-54-28.png)
   
* The Docker Daemon

  The Docker daemon is the *`hub of your interactions with Docker`*, which waits for Docker API requests and manages the state of your Docker objects accordingly

  ![](2022-08-20-18-59-07.png)

* The Docker Client

  The Docker client is the simplest component in the Docker architecture, which *`helps Docker users to communicate with Docker`*

  ![](2022-08-20-19-00-47.png)

* Docker Registries

  Docker registries provide a *`platform to store the Docker images`*

  ![](2022-08-20-19-02-23.png)

* Docker Images
  
  Images act as a *`read-only template for creating new containers`*. The platform for creating a new image is a base image

  User can create and run his own Docker image by defining steps in a *`Dockerfile`*. Each instruction written in a Dockerfile creates an additional layer in the image

  ![](2022-08-20-19-04-24.png)

* Docker Containers

   Docker container appears to be a *`lightweight form of virtual machines`*

   Docker container is created using Docker images or we can say *`container is a runnable instance of a Docker image.`*
  
  Containers hold the complete set of dependencies required by an application to run in more confined way. Using the Docker API or CLI, you can easily create, run, move, stop or remove a container

  ![](2022-08-20-19-07-21.png)
  
  Docker containers will always be in one of the four states

  ![](2022-08-20-19-09-32.png)
  
IV. Docker vs Virtual Machine

  * **Heavy weight virtuzalization**
    
    Heavy wight virtualization means each and every virtual machine *`having its own independent operating system`*

    But the problem with these software’s are that *`we need to install whole Guest OS`* also for doing small experiments

    ![](2022-08-22-09-13-07.png)

  * **Light weight virtuzalization**
    
    Nowadays, the focus is *`moving towards lightweight container based virtualization`* technology known as Docker

    With Docker, application can be placed inside the Docker container, providing *`Container as a Service (CAAS)`* platform.

    These containers *`containing application are easy to handle and can be placed on any type of platform`* including cloud

     Also these types of light weight technology are *`fast and consume almost negligible resources`*.

     ![](2022-08-22-09-15-36.png)

     In early stages, if we try to compare Docker containers to virtual machines we will see that *`Docker containers have ability to share single kernel and application library`*

     Docker container *`possess lower system overhead(i.e. computation time, memory etc.).`* Therefore performance of the application running inside a container is generally better as compared to application running within a virtual machine

     Creation and launching time of Docker container *`is much lesser`* than that of virtual machine
  
V. Advantage of Docker Container

  - Free and Open Source
    
    Docker is completely free and open source which can be easily downloaded and installed

  - Ultra Consistent

    While creating virtual machine, sufficient amount of computer resources are required and if resources provided are insufficient virtual machine will not work properly. Whereas in case of Docker, resource consumption is very less because of which higher level of consistency can be achieved

  - Rapid Deployment

    In the past, deployment of new hardware used to take few days. But with the arrival of virtual machines, timeframe was reduced down to minutes. Now the appearance of Docker has reduced deployment to mere seconds. Also the cost of creating and destroying the containers is negligible.

  - Portability
    
    Applications created inside the Docker containers are portable and light weighted. These portable applications can be treated as a single unit hence can be moved easily, without affecting the performance of the containers

  - Isolation

    Applications created inside the Docker containers are portable and light weighted. These portable applications can be treated as a single unit hence can be moved easily, without affecting the performance of the containers not only also ensures easy creation and deletion of an application but also ensures that each application only uses resources that have been assigned to them.

  - Security

    Using Docker is secure because applications and services that are running inside the containers are completely independent from each other. Which means one container cannot poke into another container. Also as discussed earlier, there is no sharing of resources between containers, and each container has its own set of required resources. That means if something goes wrong with one container, the data available in that container will be affected without affecting the rest of the containers.

VI. Disadvantage of Docker Container

* Persistent Data Storage is Complicated 
  
  In docker, when we shut down a container then all of the data residing inside that container gets disappear forever, unless you save it somewhere else first. Although, Docker provide ways to save your data persistently, such as Docker Data Volumes, but using it seamlessly is still very challenging task. 

* Compatibility with Older Machines 

  Currently, Docker is compatible only with 64-bit local machines. It does not run on 32-bit machines

* Containers don’t run at Bare Metal Speed

  There is no doubt that Docker containers are capable of consuming resources more efficiently than virtual machines. But performance of containers is not up to that level due to overlay networking

* Platform Dependant 

  Docker was initially designed to run on Linux machine. However, nowadays Docker has started supporting Windows, Mac OS X and many other platforms. To make Docker platform independent we will need additional layer between host operating system and Docker

VII. Virtualization vs Containers
  
  * Heavy Weight Virtuzalization vs Light Weight Virtuzalization
    
    Table 1 compared heavy weight virtualization i.e. Virtual Box against light weight virtualization i.e. Docker on factors like size, memory, storage, installation time and boot time

    ![](2022-08-22-09-40-59.png)

  * Virtuzalization vs Container

    Table 2, Container starts within a few seconds with faster speed and capable of loading more services by using less space

    ![](2022-08-22-09-41-43.png)

  * Virtual Machine vs Containers

    Table 3 compares Virtual Machines and Containers on multiple factors like performance, isolation security, networking, storage.

    ![](2022-08-22-09-42-18.png)

VIII. Conclusion and Future Scope

  Microservices architecture is a core concept around which Docker is built. This concept has reduced the building and deployment time of Docker containers. Also the demand for resources by a Docker container is very low, and it can achieve higher performance. Docker provides some fine features, which guarantee simplified usability and scalability. However, the Docker container still has some issues regarding security, but tools can be applied to strengthen the base of the container. The main purpose of writing this paper was to give users a brief overview about all the important components of Docker and how Docker technology is different from pre-existing technologies. In future, we will try to look into the other features of Docker like Docker Swarm; a technology for managing a cluster of Docker Engines [15] and NFS Storage; a technology to store and update files on remote computers [16]. We believe, with time Docker technology will get matured and will be deployed more widely. 

## **Docker Run Hello  World**
![](2022-08-28-01-26-47.png)