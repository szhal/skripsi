## **What is Jenkins?**
Jenkins is a tool used for automation with continuous integration and continuous deployment pipelines.

## **Why to use Jenkins as an App Developer?**
It allows you to automatically watch for certain events in your repository and react to those events. You can do things like:
- build your code
- run scripts
- perform testing
- deploy app if everything passess, OR
- reject the deployment if the test fails
- see logs of every step throughout the jenkins pipeline and the result of the deployment

## **Benefit using Jenkins**

Jenkins can ensure that all of your tests are run in the same environment or as many environment as you want

Another benefit is that Jenkins has a very large community that is amassed over almost the last 20 years so there a lot of tutorials and community questions and answers that you can find, another feature of Jenkins is its plugin architecture to use Jenkins effectively, this plugins can include compiling or testing.

Jenkins is self-hosted by default, you don't need to pay for an enterprise tier.

## **What are the benefits using Jenkins versus other alternatives**


