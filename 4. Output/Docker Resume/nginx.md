*Nginx*

1  server {
2   listen 80 default_server; 
3   server_name www.example.com; 
4   location / {
5   root /usr/share/nginx/html;
6   # alias /usr/share/nginx/html;
7   index index.html index.htm;
8   }
9  }

1 server block. define new context for NGINX to listen for.
2 instruct NGINX to listen on port 80, parameter default_server instruct NGINX to use this server as the default context for port 80
3 server_name defines the hostname or the names of the requests that should be directed to this server
4 location block. defines a configuration based on the path in the URL
5 the root shows NGINX where to look for static files when serving content for the given context
6 
7 index directive provides NGINX with a default file, in the event that no further path is provided in the URI


